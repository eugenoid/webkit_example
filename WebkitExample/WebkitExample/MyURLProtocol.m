//
//  MyURLProtocol.m
//  WebkitExample
//
//  Created by Eugene Migun on 1/5/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import "MyURLProtocol.h"

@implementation MyURLProtocol

+ (BOOL)canInitWithRequest:(NSURLRequest *)request
{
	static NSUInteger requestCount = 0;
	NSLog(@"Request #%lu: URL = %@", (unsigned long)requestCount++, request.URL.absoluteString);
	return NO;
}

@end
