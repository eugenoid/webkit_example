//
//  Utils.h
//  WebkitExample
//
//  Created by Eugene Migun on 1/5/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import <Foundation/Foundation.h>

void dumpObjcMethods(Class clz);
NSDictionary* ivars (Class class);
void exchangeImplementations(Class c, SEL sel, SEL newSel);
void swizzleClassMethod(Class c, SEL orig, SEL new);





@interface Utils : NSObject

@end
