//
//  AVPlayerViewControllerSwizzling.m
//  WebkitExample
//
//  Created by Eugene Migun on 1/5/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import "AVPlayerViewControllerSwizzling.h"
#import "Utils.h"
#import <objc/runtime.h>

@import AVFoundation;
@import AVKit;


@interface AVPlayerViewController (bbb)
@end

@implementation AVPlayerViewController(bbb)

+(void) load
{
	/*exchangeImplementations(self, @selector(initWithNibName:bundle:), @selector(swizzled_initWithNibName:bundle:));
	exchangeImplementations(self, @selector(player), @selector(swizzled_player));
	exchangeImplementations(self, @selector(setPlayer:), @selector(swizzled_setPlayer:));
	
	
	exchangeImplementations(self, @selector(initWithPlayerLayerView:), @selector(swizzled_initWithPlayerLayerView:));
	*/
}

-(AVPlayerViewController*) swizzled_initWithPlayerLayerView: (id) param
{
	AVPlayerViewController* result = [self swizzled_initWithPlayerLayerView: param];
	
	NSLog(@"param: %@", param);
	
	//[self addObserver: self forKeyPath: @"player" options: NSKeyValueObservingOptionNew context: nil];
	
	
	return result;
}

-(AVPlayerViewController*) swizzled_initWithNibName: (NSString*) name bundle: (NSBundle*) bundle
{
	NSLog(@"swizzled_initWithNibName: %@, bundle: %@", name, bundle);
	
	AVPlayerViewController* result = [self swizzled_initWithNibName: name bundle: bundle];
	
	NSLog(@"player: %@", result.player);
	
	return result;
}
/*
- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
						change:(NSDictionary *)change
					   context:(void *)context
{
	NSLog(@"keypath: %@, change: %@, object: %@", keyPath, change, object);
	
	
	if([keyPath isEqualToString: @"playerController.playing"])
	{
		NSLog(@"object: %@", object);
		NSLog(@"player: %@", ((AVPlayerViewController*)object).player);
		//[object valueForKey: @"aaa"];
		
		NSLog(@"ivars: %@", ivars([self class]));
		
		id controller = [self valueForKey: @"playerController"];
		NSLog(@"controller: %@", controller);
		
		NSLog(@"ivars2: %@", ivars([controller class]));
		
		NSLog(@"player: %@", [controller player]);
		
		id proxy = [controller performSelector:@selector(playerControllerProxy)];
		
		NSLog(@"playerControllerProxy: %@", proxy);
		//playerControllerProxy
		
		NSLog(@"playerasdfasdfasd: %@", [proxy player]);
		
		NSLog(@"self.class: %@", [self class]);
		NSLog(@"controller.class: %@", [controller class]);
		NSLog(@"proxy.class: %@", [proxy class]);
		
		//dumpObjcMethods([self class]); //AVPlayerViewController
		//dumpObjcMethods([controller class]); //WebAVPlayerController
		//dumpObjcMethods([proxy class]); //AVPlayerController
		
		
		//NSLog(@"controller player: %@", [controller valueForKey: @"_player"]);
		
	}
}*/

-(AVPlayer*)swizzled_player
{
	AVPlayer* result = [self swizzled_player];
	
	return result;
}

-(void) swizzled_setPlayer:(AVPlayer *)player
{
	NSLog(@"swizzled_setPlayer: %@", player);
	
	[self swizzled_setPlayer: player];
}



@end
