//
//  OtherSwizzlings.m
//  WebkitExample
//
//  Created by Admin on 05/01/2017.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import "OtherSwizzlings.h"
#import "Utils.h"
#import <objc/runtime.h>

@import UIKit;
@import AVFoundation;
@import AVKit;

//const char* sssss = "player";

//static const NSString* s = @"asdf";

@interface NSObject (ooo)
@end

@implementation NSObject(ooo)

+(void) load
{
	//exchangeImplementations(self, @selector(init), @selector(swizzled_init));
}

-(NSObject*) swizzled_init
{
	NSObject* result = [self swizzled_init];
	
	NSString* className = [[NSString stringWithUTF8String: class_getName([self class])] copy];
	NSLog(@"swizzled_init: %@", className);
	
	return result;
}


@end


@interface UIView (vvv)
@end

@implementation UIView(vvv)

+(void) load
{
	//exchangeImplementations(self, @selector(didAddSubview:), @selector(swizzled_didAddSubview:));
}

-(void) swizzled_didAddSubview: (UIView*) subview
{
	[self swizzled_didAddSubview: subview];

	if(self.class == NSClassFromString(@"AVPlayerView"))
	{
		NSLog(@"---------- AVPlayerView detected");
	}
}

@end

@interface UIViewController (vvv1)
@end

@implementation UIViewController(vvv1)

+(void) load
{
	exchangeImplementations(self, @selector(viewWillAppear:), @selector(swizzled_viewWillAppear:));
	exchangeImplementations(self, @selector(viewDidAppear:), @selector(swizzled_viewDidAppear:));
	exchangeImplementations(self, @selector(viewWillDisappear:), @selector(swizzled_viewWillDisappear:));
	exchangeImplementations(self, @selector(viewDidDisappear:), @selector(swizzled_viewDidDisappear:));
}

-(void) swizzled_viewWillAppear: (BOOL) animated
{
	[self swizzled_viewWillAppear: animated];
	if(self.class == [AVPlayerViewController class])
	{
		[[NSNotificationCenter defaultCenter] postNotificationName: @"PLAYER_WILL_APPEAR" object: self];
	}
}

-(void) swizzled_viewDidAppear: (BOOL) animated
{
	[self swizzled_viewDidAppear: animated];
	
	if(self.class == [AVPlayerViewController class])
	{
		[[NSNotificationCenter defaultCenter] postNotificationName: @"PLAYER_DID_APPEAR" object: self];
	}
}


- (void)printSubviewsWithIndentation:(int)indentation view: (UIView*) view
{
	NSArray *subviews = [view subviews];
	
	for (int i = 0; i < [subviews count]; i++)
	{
		UIView *currentSubview = [subviews objectAtIndex:i];
		
		NSMutableString *currentViewDescription = [[NSMutableString alloc] init];
		
		for (int j = 0; j <= indentation; j++)
		{
			[currentViewDescription appendString:@"   "];   // indent the description
		}
		
		// print whatever you want to know about the subview
		[currentViewDescription appendFormat:@"[%d]: class: '%@'", i, NSStringFromClass([currentSubview class])];
		
		//NSLog(@"currentView.class: %@", currentSubview.class);
		
		if(currentSubview.class == NSClassFromString(@"AVForceButton"))
		{
			//currentSubview.backgroundColor = [UIColor blueColor];
			
			//UIView* v = [[UIView alloc] initWithFrame:CGRectMake(0,0,40,40)];
			//v.backgroundColor = [UIColor greenColor];
			//[currentSubview.superview addSubview: v];
			
			
		}
		
		if(currentSubview.class == NSClassFromString(@"_UIBackdropContentView"))
		{
			for(UIView* v in currentSubview.subviews)
			{
				if(v.class == NSClassFromString(@"AVForceButton"))
				{
					v.backgroundColor = [UIColor redColor];
					
					//UIView* v = [[UIView alloc] initWithFrame:CGRectMake(0,0,40,40)];
					//v.backgroundColor = [UIColor greenColor];
					//[currentSubview.superview addSubview: v];
				}
			}
			
			
		}
		
		
		// Log the description string to the console
		NSLog(@"%@", currentViewDescription);
		
		[self printSubviewsWithIndentation:indentation+1 view: currentSubview];
	}
}

-(void) swizzled_viewWillDisappear: (BOOL) animated
{
	[self swizzled_viewWillDisappear: animated];
	
	if(self.class == [AVPlayerViewController class])
	{
		//NSLog(@"---------- AVPlayerViewController disappeared [%p]", self);
		[[NSNotificationCenter defaultCenter] postNotificationName: @"PLAYER_WILL_DISAPPEAR" object: self];
	}
}

-(void) swizzled_viewDidDisappear: (BOOL) animated
{
	[self swizzled_viewDidDisappear: animated];
	
	if(self.class == [AVPlayerViewController class])
	{
		//NSLog(@"---------- AVPlayerViewController disappeared [%p]", self);
		[[NSNotificationCenter defaultCenter] postNotificationName: @"PLAYER_DID_DISAPPEAR" object: self];
	}
}

@end




@implementation OtherSwizzlings


@end
