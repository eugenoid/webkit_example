//
//  Utils.m
//  WebkitExample
//
//  Created by Eugene Migun on 1/5/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import "Utils.h"
#import <objc/runtime.h>


void dumpObjcMethods(Class clz)
{
	unsigned int methodCount = 0;
	Method *methods = class_copyMethodList(clz, &methodCount);
	
	printf("Found %d methods on '%s'\n", methodCount, class_getName(clz));
	
	for (unsigned int i = 0; i < methodCount; i++) {
		Method method = methods[i];
		
		printf("\t'%s' has method named '%s' of encoding '%s'\n",
			   class_getName(clz),
			   sel_getName(method_getName(method)),
			   method_getTypeEncoding(method));
		
		/**
		 *  Or do whatever you need here...
		 */
	}
	
	free(methods);
}

NSDictionary* ivars (Class class)
{
	NSMutableDictionary* ivarsDict=[NSMutableDictionary new];
	unsigned int count;
	Ivar* ivars=class_copyIvarList(class, &count);
	for(int i=0; i<count; i++)
	{
		Ivar ivar= ivars[i];
		const char* name = ivar_getName(ivar);
		const char* typeEncoding = ivar_getTypeEncoding(ivar);
		[ivarsDict setObject: [NSString stringWithFormat: @"%s",typeEncoding] forKey: [NSString stringWithFormat: @"%s",name]];
	}
	free(ivars);
	return ivarsDict;
}

void exchangeImplementations(Class c, SEL sel, SEL newSel)
{
	Method m1 = class_getInstanceMethod(c, sel);
	Method m2 = class_getInstanceMethod(c, newSel);
	if(m1)
		method_exchangeImplementations(m1, m2);
}

void swizzleClassMethod(Class c, SEL orig, SEL new)
{
	Method origMethod = class_getClassMethod(c, orig);
	Method newMethod = class_getClassMethod(c, new);
	
	c = object_getClass((id)c);
	
	if(class_addMethod(c, orig, method_getImplementation(newMethod), method_getTypeEncoding(newMethod)))
		class_replaceMethod(c, new, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
	else
		method_exchangeImplementations(origMethod, newMethod);
}



@implementation Utils




@end
