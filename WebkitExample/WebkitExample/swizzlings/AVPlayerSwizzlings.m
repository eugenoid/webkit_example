//
//  AVPlayerSwizzlings.m
//  WebkitExample
//
//  Created by Eugene Migun on 1/5/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import "AVPlayerSwizzlings.h"
#import "Utils.h"
#import <objc/runtime.h>


@import AVFoundation;



static NSString* lastPlayedURL = nil; //храню строку, а не URL, чтобы сделать [... copy]
static BOOL adIsBeingShown = NO;


@interface AVPlayer (ext)
@end

@implementation AVPlayer(ext)


+(void) load
{/*
	exchangeImplementations(self, @selector(play), @selector(swizzled_play));
	exchangeImplementations(self, @selector(pause), @selector(swizzled_pause));
	exchangeImplementations(self, @selector(setRate:), @selector(swizzled_setRate:));
	exchangeImplementations(self, @selector(rate), @selector(swizzled_rate));
	
	
	exchangeImplementations(self, @selector(initWithURL:), @selector(swizzled_initWithURL:));
	exchangeImplementations(self, @selector(initWithPlayerItem:), @selector(swizzled_initWithPlayerItem:));
	
	swizzleClassMethod(self, @selector(playerWithURL:), @selector(swizzled_playerWithURL:));
	swizzleClassMethod(self, @selector(playerWithPlayerItem:), @selector(swizzled_playerWithPlayerItem:));*/
}

-(AVPlayer*) swizzled_init
{
	return nil;
}

- (instancetype)swizzled_initWithURL:(NSURL *)URL
{
	return nil;
}

+ (instancetype)swizzled_playerWithURL:(NSURL *)URL
{
	return nil;
}

- (instancetype)swizzled_initWithPlayerItem:(AVPlayerItem *)item
{
	return nil;
}

+ (instancetype)swizzled_playerWithPlayerItem:(AVPlayerItem *)item
{
	return nil;
}

-(void) swizzled_rate
{
	NSLog(@"swizzled_rate: %@", self.currentItem);
	
	[self swizzled_rate];
}

-(void) swizzled_play
{
	NSLog(@"swizzled_play: %@", self.currentItem);
	
	[self swizzled_play];
}

-(void) swizzled_pause
{
	NSLog(@"swizzled_pause: %@", self.currentItem);
	
	[self swizzled_pause];
}

-(void) swizzled_setRate: (float) rate
{
	NSLog(@"swizzled_setRate: %f", rate);
	
	[self swizzled_setRate: rate];
	
	if(rate < 0.001) // == 0 (stop)
	{
		//BPLogRed(@"STOP: %@", self.currentItem);
		lastPlayedURL = nil;
	}
	else if(rate > 0.999) // == 1 (play)
	{
		//BPLogRed(@"PLAY: %@", self.currentItem);
		//DBLog(@"adIsBeingShown: %i", adIsBeingShown);
		
		if(!adIsBeingShown)
		{
			if([self.currentItem.asset isKindOfClass: [AVURLAsset class]])
			{
				NSURL* itemURL = ((AVURLAsset*)(self.currentItem.asset)).URL;
				//DBLog(@"itemURL: %@", itemURL);
				//DBLog(@"lastPlayerURL: %@", lastPlayedURL);
				
				if(!lastPlayedURL)
				{
					lastPlayedURL = [[itemURL absoluteString] copy];
					//DBLog(@"lastPlayedURL: %@", lastPlayedURL);
					[[NSNotificationCenter defaultCenter] postNotificationName: @"URL_READY" object: self userInfo: @{@"URL":lastPlayedURL}];
				}
				else
				{
					if(![[itemURL absoluteString] isEqualToString: lastPlayedURL])
					{
						lastPlayedURL = [[itemURL absoluteString] copy];
						//DBLog(@"lastPlayedURL: %@", lastPlayedURL);
						[[NSNotificationCenter defaultCenter] postNotificationName: @"URL_READY" object: self userInfo: @{@"URL":lastPlayedURL}];
					}
				}
			}
		}
	}
}

@end

