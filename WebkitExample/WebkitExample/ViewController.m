//
//  ViewController.m
//  WebkitExample
//
//  Created by Eugene Migun on 1/4/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import "ViewController.h"
#import <objc/runtime.h>
#import <objc/message.h>

@import WebKit;
@import SafariServices;
@import AVFoundation;
@import AVKit;



void dumpClassMethods (Class cls)
{
	unsigned int methodCount = 0;
	Method *methods = class_copyMethodList(cls, &methodCount);
	
	printf("Found %d methods on '%s'\n", methodCount, class_getName(cls));
	
	for (unsigned int i = 0; i < methodCount; i++)
	{
		Method method = methods[i];
		
		printf("\t'%s' has method named '%s' of encoding '%s'\n",
			   class_getName(cls),
			   sel_getName(method_getName(method)),
			   method_getTypeEncoding(method));
		
		/**
		 *  Or do whatever you need here...
		 */
	}
	
	free(methods);
}


//-(type) function
#define encode_return(type)																[[NSString stringWithFormat: @"%s@:", @encode(type)] UTF8String]
//-(returnType) function: (paramType) param
#define encode_return_param(returnType, paramType)										[[NSString stringWithFormat: @"%s@:%s", @encode(returnType), @encode(paramType)] UTF8String]
#define encode_return_param_param(returnType, paramType1, paramType2)					[[NSString stringWithFormat: @"%s@:%s%s", @encode(returnType), @encode(paramType1), @encode(paramType2)] UTF8String]
#define encode_return_param_param_param(returnType, paramType1, paramType2, paramType3)	[[NSString stringWithFormat: @"%s@:%s%s%s", @encode(returnType), @encode(paramType1), @encode(paramType2), @encode(paramType3)] UTF8String]


@interface ViewController () </*WKUIDelegate,*/ UIGestureRecognizerDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UIView* webViewBackground;
//@property (strong, nonatomic) WKWebView* webView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end


#pragma mark - WKWebView Delegate

static void didStartProvisionalNavigation(id self, SEL _cmd, WKWebView* webView, WKNavigation* navigation)
{
	NSLog(@"didStartProvisionalNavigation");
}

static void didCommitNavigation(id self, SEL _cmd, WKWebView* webView, WKNavigation* navigation)
{
	NSLog(@"didCommitNavigation: %@", navigation);
}

static void didReceiveServerRedirectForProvisionalNavigation(id self, SEL _cmd, WKWebView* webView, WKNavigation* navigation)
{
	NSLog(@"didReceiveServerRedirectForProvisionalNavigation: %@", navigation);
}

static void didFailNavigation(id self, SEL _cmd, WKWebView* webView, WKNavigation* navigation, NSError* error)
{
	NSLog(@"didFailNavigation: %@ withError: %@", navigation, error);
}

static void didFailProvisionalNavigation(id self, SEL _cmd, WKWebView* webView, WKNavigation* navigation, NSError* error)
{
	NSLog(@"didFailProvisionalNavigation: %@ withError: %@", navigation, error);
}

static void didFinishNavigation(ViewController* self, SEL _cmd, WKWebView* webView, WKNavigation* navigation)
{
	NSLog(@"didFinishNavigation: %@", navigation);
	
	self.searchBar.text = [webView.URL absoluteString];
	[self.searchBar resignFirstResponder];
}

static void webViewWebContentProcessDidTerminate(id self, SEL _cmd, WKWebView* webView)
{
	NSLog(@"webViewWebContentProcessDidTerminate");
}

typedef void (^ actionHandler)(WKNavigationActionPolicy);
static void decidePolicyForNavigationAction(id self, SEL _cmd, WKWebView* webView, WKNavigationAction* navigationAction, actionHandler decisionHandler)
{
	NSLog(@"decidePolicyForNavigationAction: %@", navigationAction);
	decisionHandler(WKNavigationActionPolicyAllow);
}

typedef void (^ responseHandler)(WKNavigationResponsePolicy);
static void decidePolicyForNavigationResponse(id self, SEL _cmd, WKWebView* webView, WKNavigationResponse* navigationResponse, responseHandler decisionHandler)
{
	NSLog(@"decidePolicyForNavigationResponse: %@", navigationResponse);
	decisionHandler(WKNavigationResponsePolicyAllow);
}

typedef void (^ challengeHandler)(NSURLSessionAuthChallengeDisposition, NSURLCredential*);
static void didReceiveAuthenticationChallenge(id self, SEL _cmd, WKWebView* webView, NSURLAuthenticationChallenge* challenge, challengeHandler completionHandler)
{
	NSLog(@"didReceiveAuthenticationChallenge: %@", challenge);
	NSLog(@"protectionSpace: %@", challenge.protectionSpace);
	completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]);
}


#pragma mark - ViewController


static char kWebView;
static WKWebView* webView (ViewController* self)
{
	return objc_getAssociatedObject(self, &kWebView);
}
static void setWebView(ViewController* self, WKWebView* newValue)
{
	objc_setAssociatedObject(self, &kWebView, newValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

static void createWKWebView(ViewController* self)
{
	NSString *source = @"var style = document.createElement('style'); \
	style.type = 'text/css'; \
	style.innerText = '*:not(input):not(textarea) { -webkit-touch-callout: none; }'; \
	var head = document.getElementsByTagName('head')[0];\
	head.appendChild(style);";
	WKUserScript *script = [[WKUserScript alloc] initWithSource:source injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
 
	// Create the user content controller and add the script to it
	WKUserContentController *userContentController = [WKUserContentController new];
	[userContentController addUserScript:script];
	WKWebViewConfiguration *configuration = [WKWebViewConfiguration new];
	configuration.userContentController = userContentController;
	
	setWebView(self, [[WKWebView alloc] initWithFrame: CGRectZero configuration: configuration]);
	
	webView(self).allowsBackForwardNavigationGestures = YES;
	
	webView(self).allowsBackForwardNavigationGestures = YES;
	webView(self).navigationDelegate = self;
	[self.webViewBackground addSubview: webView(self)];
	
	UILongPressGestureRecognizer* recognizer =	[[UILongPressGestureRecognizer alloc] initWithTarget:self
																							 action:@selector(handleLongPressGestureRecognizer:)];
	recognizer.delegate = self;
	[webView(self) addGestureRecognizer:recognizer];
}




@implementation ViewController

+ (void) load
{
	NSLog(@"encode: %s", encode_return_param_param(void, WKWebView*, WKNavigation*));
	
	dumpClassMethods(self);
	class_addMethod(self, NSSelectorFromString(@"webView:didStartProvisionalNavigation:"), (IMP)didStartProvisionalNavigation, encode_return_param_param(void, WKWebView*, WKNavigation*));
	class_addMethod(self, NSSelectorFromString(@"webView:didCommitNavigation:"), (IMP)didCommitNavigation, encode_return_param_param(void, WKWebView*, WKNavigation*));
	class_addMethod(self, NSSelectorFromString(@"webView:didReceiveServerRedirectForProvisionalNavigation:"), (IMP)didReceiveServerRedirectForProvisionalNavigation, encode_return_param_param(void, WKWebView*, WKNavigation*));
	class_addMethod(self, NSSelectorFromString(@"webView:didFailNavigation:error:"), (IMP)didFailNavigation, encode_return_param_param_param(void, WKWebView*, WKNavigation*, NSError*));
	class_addMethod(self, NSSelectorFromString(@"webView:didFailProvisionalNavigation:error:"), (IMP)didFailProvisionalNavigation, encode_return_param_param_param(void, WKWebView*, WKNavigation*, NSError*));
	class_addMethod(self, NSSelectorFromString(@"webView:didFinishNavigation:"), (IMP)didFinishNavigation, encode_return_param_param(void, WKWebView*, WKNavigation*));
	class_addMethod(self, NSSelectorFromString(@"webViewWebContentProcessDidTerminate:"), (IMP)webViewWebContentProcessDidTerminate, encode_return_param(void, WKWebView*));
	class_addMethod(self, NSSelectorFromString(@"webView:decidePolicyForNavigationAction:decisionHandler:"), (IMP)decidePolicyForNavigationAction, encode_return_param_param_param(void, WKWebView*, WKNavigationAction*, actionHandler));
	class_addMethod(self, NSSelectorFromString(@"webView:decidePolicyForNavigationResponse:decisionHandler:"), (IMP)decidePolicyForNavigationResponse, encode_return_param_param_param(void, WKWebView*, WKNavigationResponse*, actionHandler));
	class_addMethod(self, NSSelectorFromString(@"webView:didReceiveAuthenticationChallenge:completionHandler:"), (IMP)didReceiveAuthenticationChallenge, encode_return_param_param_param(void, WKWebView*, NSURLAuthenticationChallenge*, challengeHandler));
	
	dumpClassMethods(self);
}



- (void)viewDidLoad
{
	[super viewDidLoad];
	
	createWKWebView(self);
	
	
	//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationHandler:) name:nil object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPlayerDidAppear:) name:@"PLAYER_DID_APPEAR" object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPlayerDidDisappear:) name:@"PLAYER_DID_DISAPPEAR" object:nil];

	
	self.searchBar.delegate = self;
	
	/*
	int numClasses;
	Class * classes = NULL;
	
	classes = NULL;
	numClasses = objc_getClassList(NULL, 0);
	
	if (numClasses > 0 )
	{
		classes = (__unsafe_unretained Class *)malloc(sizeof(Class) * numClasses);
		numClasses = objc_getClassList(classes, numClasses);
		for (int i = 0; i < numClasses; i++) {
			Class c = classes[i];
			
			NSString* className = [NSString stringWithUTF8String: class_getName(c)];
			
			if([[className lowercaseString] rangeOfString:@"player"].location != NSNotFound)
				NSLog(@"%s", class_getName(c));
		}
		free(classes);
	}
	*/

	//[self replaceGMSUISettingsViewImplementation];
}

-(void) notificationHandler: (NSNotification*) notification
{
	NSLog(@"notification: %@", notification.name);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer*)otherGestureRecognizer
{
	return YES;
}

- (void)handleLongPressGestureRecognizer: (UILongPressGestureRecognizer*)recognizer
{
	if (recognizer.state == UIGestureRecognizerStateBegan)
	{
		[self loadJavaScriptFileIfNeededWithCompletion:^
		{
			CGPoint touchPoint = [recognizer locationInView:webView(self)];
			[self imageSourceFromPoint:touchPoint completion:^(NSString* source)
			{
				NSLog(@"source: %@", source);
				
				[self actionWithURL: source];
			}];
		}];
	}
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	NSLog(@"Search clicked: %@", searchBar.text);
	
	NSString* url = searchBar.text;
	
	if([url hasPrefix: @"http://"] || [url hasPrefix: @"https://"])
	{
		[webView(self) loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: url]]];
	}
	else
	{
		url = [NSString stringWithFormat: @"https://%@", url];
		[webView(self) loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: url]]];
	}
		
	
	
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	
}




-(void) actionWithURL: (NSString*) url
{
	NSLog(@"actionWithURL: %@", url);
	
	UIAlertController* actionSheet = [UIAlertController alertControllerWithTitle: @"bla bla bla"
																		 message: url
																  preferredStyle: UIAlertControllerStyleActionSheet];
	
	[actionSheet addAction: [UIAlertAction actionWithTitle: @"copy"
													 style: UIAlertActionStyleDefault
												   handler:^(UIAlertAction * _Nonnull action)
	{
		NSLog(@"copy");
	}]];

	[actionSheet addAction: [UIAlertAction actionWithTitle: @"download"
													 style: UIAlertActionStyleDefault
												   handler:^(UIAlertAction * _Nonnull action)
	{
		NSLog(@"download");
	}]];
	
	[actionSheet addAction: [UIAlertAction actionWithTitle: @"Cancel"
													 style: UIAlertActionStyleCancel
												   handler:^(UIAlertAction * _Nonnull action)
							 {
								 NSLog(@"cancel");
							 }]];

	[self presentViewController: actionSheet animated: YES completion:^
	{
		NSLog(@"present handler");
	}];
}

- (void)loadJavaScriptFileIfNeededWithCompletion:(void(^)())completion
{
	[webView(self) evaluateJavaScript:[self toolsJavaScriptFile]
				   completionHandler:^(id result, NSError* error)
	{
		completion();
	}];
}

- (NSString*)toolsJavaScriptFile
{
	NSString* path = [[NSBundle mainBundle] pathForResource:@"tools" ofType:@"js"];
	return [NSString stringWithContentsOfFile:path
									 encoding:NSUTF8StringEncoding
										error:nil];
}

- (void)imageSourceFromPoint:(CGPoint)point
				  completion:(void(^)(NSString*))completion
{
	//NSString* jsCode = [NSString stringWithFormat: @"imageSourceFromPoint(%g, %g)", point.x, point.y];
	NSString* jsCode = [NSString stringWithFormat: @"getLinkAtPoint(%g, %g)", point.x, point.y];
	[webView(self) evaluateJavaScript:jsCode
				   completionHandler:^(id result, NSError* error)
	{
	   NSString* resultString = (NSString*)result;
	   completion(resultString);
	}];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear: animated];

	//[self loadSafariViewController: [NSURL URLWithString: @"https://tut.by"]];
	
	webView(self).frame = self.webViewBackground.bounds;
	[self loadWKWebViewRequest: @"https://kibergrad.fm/934/nicki-minaj"];
	
	
	//self.webView2.frame = self.webViewBackground.bounds;
	
	//[self loadUIWebViewRequest: @"https://youtube.com"];
}

-(void) loadWKWebViewRequest: (NSString*) url
{
	WKNavigation* navigation = [webView(self) loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: url]]];
	NSLog(@"navigation: %@", navigation);
}

-(void) loadSafariViewController: (NSURL*) url
{
	SFSafariViewController* safariViewController = [[SFSafariViewController alloc] initWithURL: url];
	//safariViewController.delegate = self;
	[self presentViewController: safariViewController animated: YES completion: nil];
}

-(void)onPlayerDidAppear:(NSNotification*) notification
{
	NSLog(@"---- onPlayerDidAppear: %@", notification);
	
	[webView(self) evaluateJavaScript:@"document.querySelector('video').currentSrc;" completionHandler:^(id result, NSError *error)
	 {
		 NSString* videoURL = result;
	
		 if(!videoURL)
		 {
			 NSLog(@"----------- ERROR: video url not found");
			 return;
		 }
		 
		 NSLog(@"videoURL: %@", videoURL);
		 [self videoUrlReady: videoURL viewController: notification.object];
	 }];
}

-(void)onPlayerDidDisappear:(NSNotification*) notification
{
	NSLog(@"---- onPlayerDidDisappear: %@", notification);
	
	g_videoURL = nil;
	g_playerVC = nil;
}


-(void)videoUrlReady: (NSString*) videoURL viewController: (UIViewController*) viewController
{
	NSLog(@"urlReady: %@, viewController: %@", videoURL, viewController);

	g_videoURL = [videoURL copy];
	g_playerVC = viewController;
	
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
	{
		g_forceButton = nil;
		[self findAVForceButton: viewController.view.window];
					   
		if(g_forceButton)
		{
			g_forceButton.backgroundColor = [UIColor redColor];
			
			UIView* toolbar = g_forceButton.superview.superview;
			
			UIButton* mybutton = [[UIButton alloc] initWithFrame: CGRectMake(0,0,40,40)];
			mybutton.backgroundColor = [UIColor greenColor];
						   
			[mybutton addTarget: self action:@selector(onButton:) forControlEvents: UIControlEventTouchUpInside];
						   
			[toolbar addSubview: mybutton];
		}
	});
}

static UIViewController* g_playerVC = nil;
static NSString* g_videoURL = nil;

-(void) onButton: (id) param
{
	NSLog(@"onButton");
	
	if(!g_videoURL)
	{
		NSLog(@"---- ERROR: g_videoURL == nil");
		return;
	}
	
	if(!g_playerVC)
	{
		NSLog(@"---- ERROR: g_playerVC == nil");
		return;
	}
	
	 UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"URL" message: g_videoURL preferredStyle: UIAlertControllerStyleAlert];
	 [alert addAction: [UIAlertAction actionWithTitle: @"Download" style: UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action)
	{
								NSLog(@"download: %@", g_videoURL);
							}]];
	[alert addAction: [UIAlertAction actionWithTitle: @"Cancel" style: UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action)
							{
								NSLog(@"cancel");
							}]];

	//[g_playerVC presentViewController: alert animated: YES completion: nil];
	

	[g_playerVC showViewController: alert sender: nil];
	
}


static UIView* g_forceButton = nil;

-(void) findAVForceButton: (UIView*) view
{
	if(view.class == NSClassFromString(@"AVForceButton"))
	{
		if(g_forceButton)
		{
			if(g_forceButton && view.frame.origin.x < g_forceButton.frame.origin.x)
				g_forceButton = view;
		}
		else
		{
			g_forceButton = view;
		}
		
		return;
	}
	
	NSArray *subviews = [view subviews];
	
	for (int i = 0; i < [subviews count]; i++)
	{
		UIView* subview = [subviews objectAtIndex:i];
		[self findAVForceButton: subview];
	}
}


@end
