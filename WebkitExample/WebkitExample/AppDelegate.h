//
//  AppDelegate.h
//  WebkitExample
//
//  Created by Eugene Migun on 1/4/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

