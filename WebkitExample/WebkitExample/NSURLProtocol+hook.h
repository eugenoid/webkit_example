//
//  NSURLProtocol.h
//  WebkitExample
//
//  Created by Eugene Migun on 1/5/17.
//  Copyright © 2017 Eugene Migun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLProtocol(hook)

+ (void)wk_registerScheme:(NSString *)scheme;
+ (void)wk_unregisterScheme:(NSString *)scheme;

@end
