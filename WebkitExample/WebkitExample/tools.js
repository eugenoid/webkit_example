function imageSourceFromPoint(x, y)
{
	var element = document.elementFromPoint(x, y);
	
	if(element)
		return element.innerHTML;
	
	//if ((element.tagName == 'IMG' || element.tagName == 'img') && element.src)
	//{
	//	return element.src;
	//}

	if ((element.tagName == 'A' || element.tagName == 'a'))
	{
		return element.href;
	}

	return null;
}

function getLinkAtPoint(x,y)
{
	var e = document.elementFromPoint(x,y);
	while (e)
	{
		if (e.tagName)
		{
			if(e.tagName == 'a' || e.tagName == 'A')
				return e.href;
		}
		e = e.parentNode;
	}
	return '';
}
